# -*- coding: utf-8 -*-
################################################################################
# This file is part of bookman.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################

import unittest

from bookman import FirefoxBookmarkTransformator
from bookman.models import Tag
from bookman.models import Bookmark

class FirefoxBookmarkTransformatorTest(unittest.TestCase):
    BOOKMARKS_SIMPLE = [
        Bookmark(name = 'Recent Tags',
                 url = 'place:type=6&sort=14&maxResults=10',
                 tags = [ ]),
        Bookmark(name = 'Help and Tutorials',
                 url = 'https://www.mozilla.org/en-US/firefox/help/',
                 tags = [ ]),
        Bookmark(name = 'Customize Firefox',
                 url = 'https://www.mozilla.org/en-US/firefox/customize/',
                 tags = [
                     Tag(name = 'MozillaFirefox')
                ]),
        Bookmark(name = 'Get Involved',
                 url = 'https://www.mozilla.org/en-US/contribute/',
                 tags = [
                     Tag(name = 'MozillaFirefox')
                ]),
        Bookmark(name = 'About Us',
                 url = 'https://www.mozilla.org/en-US/about/',
                 tags = [
                     Tag(name = 'MozillaFirefox')
                ]),
        Bookmark(name = 'Most Visited',
                 url = 'place:sort=8&maxResults=10',
                 tags = [
                     Tag(name = 'BookmarksToolbar')
                ]),
        Bookmark(name = 'Getting Started',
                 url = 'https://www.mozilla.org/en-US/firefox/central/',
                 tags = [
                     Tag(name = 'BookmarksToolbar')
                ])
    ]
    
    
    def test_get_bookmarks(self):
        transformator = FirefoxBookmarkTransformator("tests/bookmarks_simple.html")
        bookmarks = transformator.get_bookmarks()
        self.compare_bookmarks(bookmarks, FirefoxBookmarkTransformatorTest.BOOKMARKS_SIMPLE)
            
            
    def compare_bookmarks(self, acts, exps):
        for i in range(len(exps)):
            pos = str(i)
            act = None
            try:
                act = acts[i]
            except:
                self.fail("Expected bookmark missing at: " + pos)
                
            exp = exps[i] 
            
            self.assertEqual(act.name, exp.name, 'Expected bookmark name does not equal at: ' + pos)
            self.assertEqual(act.url, exp.url, 'Expected bookmark URL does not equal at: ' + pos)
            self.compare_tags(exp.name, act.tags, exp.tags)
            
    
    def compare_tags(self, bookmark_name, acts, exps):
        for i in range(len(exps)):
            pos = str(i)
            act = None
            try:
                act = acts[i]
            except:
                self.fail("Expected tag missing for: " + bookmark_name)
            
            exp = exps[i] 
            
            self.assertEqual(act.name, exp.name, 'Expected tag name does not equal for: ' + bookmark_name)
            
 
if __name__ == '__main__':
    unittest.main()
    
     
    