# 2018-10-14 - Release 0.5

* A default bookmark database file can be set.
* New attribute for bookmarks: an open with executable. With this single bookmarks can be opened by a different application then the global preference.
* The list of currently available tags can be viewed.
* Fedora/RPM packages can be built.

**This release breaks compatility with existing databases. To migrate an existing database please perform the following steps:**
1. Open a Terminal 
2. Execute the following command: `sqlite3 /PATH/TO/DATABASE.DB "alter table bookmark add column open_with text(1024); update bookmark set open_with = '';"` where you have to substitute `/PATH/TO/DATABASE.DB` with the real full qualified path to your database file

# 2018-09-26 - Release v0.4

* Adds missing translations.
* Fixes search function: all search words have to apply to a bookmark to be shown.
* Adds automatically backup creation on saving. 
* Fixes a tagging bug that resulted in deleting the entire database.
* Displays an error popup when the database couldn't be saved to the disk.

# 2018-09-23 - Release v0.3.1

* Filtration has to be confirmed by a hit on "Enter".
* Application was unable to be used with more than 50 bookmarks. This has been
  fixed.
* Failing to open a non-database file lead to an application crash. That
  exception is now caught.

# 2018-09-15 - Release v0.3

* Changes the license from GPLv2 to MIT.
* Supplies a .desktop file for GUI menus in Linux distros.
* Bookmarks in the Firefox HTML Export format can be imported.

# 2018-08-30 - Release v0.2

* When an URL is pasted the TreeView is refreshed automatically.
* Fix for the double click process spawning.
* The resource file search tries different paths (user, local and system level).
* The project is available on PyPI


# 2018-08-28 - Release v0.1

* It is possible to create bookmarks with Name, URL and description
* Changes are automatically detected and the application suggests to save them.
* Changes can be saved to a local database file.
* Bookmarks can be opened by a double click on the corresponding line in the 
  TreeView.
* Which application is executed on the double click on a bookmark can be 
  customized in the Preferences dialog.
* Bookmarks can be tagged.
* The TreeView can be filtered. If the entered word starts with a # then the
  word is recognized as tag. Otherwise the entered word can occur in any 
  attribute in any bookmark.
* A Debian package is ready.
